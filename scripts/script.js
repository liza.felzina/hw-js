let name = 'Liza';
let age = 23;
let number = BigInt(905749849494);
let isShown = true;
let money = null;
let salary = undefined;

console.log(`Переменная name принадлежит к типу данных ${typeof name}, хранит в себе значение ${name}`)
console.log(`Переменная age принадлежит к типу данных ${typeof age}, хранит в себе значение ${age}`)
console.log(`Переменная number принадлежит к типу данных ${typeof number}, хранит в себе значение ${number}`)
console.log(`Переменная isShown принадлежит к типу данных ${typeof isShown}, хранит в себе значение ${isShown}`)
console.log(`Переменная money принадлежит к типу данных ${typeof money}, хранит в себе значение ${money}`)
console.log(`Переменная salary принадлежит к типу данных ${typeof salary}, хранит в себе значение ${salary}`)